#ifndef OPERATIONS_GUARD
#define OPERATIONS_GUARD

#include <stdbool.h>

#include "image_utils.h"

typedef bool (*executor)(image*, int, char**);

typedef struct operation operation;
struct operation {
    char* name;
    char* usage;
    executor executor;
    operation* next;
};

operation* operations_find_by_name(char* name);
operation* operations_get();

#endif