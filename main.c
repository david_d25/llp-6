#include <stdio.h>
#include <stdlib.h>

#include "image.h"
#include "image_io.h"
#include "operations.h"
#include "image_utils.h"

void do_the_magic(char*, char*, char*, char**, int);
void print_help(char*);

int main(int arg_num, char** args) {
    if (arg_num < 4) {
        print_help(args[0]);
        return 0;
    }

    char* src = args[1];
    char* dst = args[2];
    char* operation_str = args[3];
    char** operation_args = args + 4;

    do_the_magic(src, dst, operation_str, operation_args, arg_num - 4);

    return 0;
}

void do_the_magic(char* src, char* dst, char* operation_str, char** operation_args, int args_num) {
    operation* current = operations_find_by_name(operation_str);

    if (current != NULL) {

        image* image;
        image_load_status load_status = image_load_bmp(&image, src);

        if (load_status == IMAGE_LOAD_OK) {

            if (current->executor(image, args_num, operation_args)) {
                image_save_status save_status = image_save_bmp(image, dst);

                if (save_status == IMAGE_SAVE_OK)
                    puts("Done");
                else if (save_status == IMAGE_SAVE_OPEN_FAIL)
                    puts("Failed to create new image");
                else if (save_status == IMAGE_SAVE_NO_ACCESS)
                    puts("Could not access file for writing");
                else
                    puts("Could not write new image");
            }

            image_destroy(image);

        } else if (load_status == IMAGE_LOAD_FILE_NOT_EXIST)
            puts("The specified file doesn't exist");
        else if (load_status == IMAGE_LOAD_TYPE_NOT_SUPPORTED)
            puts("Sorry, this file format is not supported");
        else if (load_status == IMAGE_LOAD_BPP_NOT_SUPPORTED)
            puts("Sorry, this bits-per-pixel number is not supported, use 24-bit images");
        else if (load_status == IMAGE_LOAD_COMPRESSION_NOT_SUPPORTED)
            puts("Sorry, image compression is not supported, use image with no compression");
        else
            puts("Failed to read image");

    } else {
        printf("Operation '%s' not found. Possible operations:\n", operation_str);
        operation* current = operations_get();
        while (current != NULL) {
            printf("- %s\n", current->usage);
            current = current->next;
        }
    }
}

void print_help(char* cmd_name) {
    printf(
        "Usage: %s <src> <dst> <operation [arguments...]>\n"
        "src        source bmp file\n"
        "dst        new bmp file\n"
        "operation  one of the following:\n",
        cmd_name
    );

    operation* current = operations_get();
    while (current != NULL) {
        printf("           - %s\n", current->usage);
        current = current->next;
    }

    printf("\nRun the tool without arguments to get help\n");
}