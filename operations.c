#include <stdio.h>
#include <string.h>
#include <math.h>
#include <stdlib.h>

#include "operations.h"

bool exec_rotate(image*, int, char**);
bool exec_blur(image*, int, char**);
bool exec_mirror(image*, int, char**);
bool exec_do_nothing(image*, int, char**);

operation rotate, blur, do_nothing, mirror;
operation* start_entry = &rotate;

operation rotate = {
    .name = "rotate",
    .usage = "rotate <degrees>",
    .executor = exec_rotate,
    .next = &blur
};

operation blur = {
    .name = "blur",
    .usage = "blur <pixels>",
    .executor = exec_blur,
    .next = &mirror
};

operation mirror = {
    .name = "mirror",
    .usage = "mirror <x | y | xy>",
    .executor = exec_mirror,
    .next = &do_nothing
};

operation do_nothing = {
    .name = "do_nothing",
    .usage = "do_nothing",
    .executor = exec_do_nothing,
    .next = 0
};

bool exec_rotate(image* img, int args_num, char** args) {
    if (args_num == 0) {
        puts("I need more arguments to run, see help for details");
        return false;
    }

    double rotation = strtod(args[0], NULL);
    printf("Rotating %.1f degrees...\n", rotation);

    image_rotate(img, rotation * M_PI / 180);
    return true;
}

bool exec_blur(image* img, int args_num, char** args) {
    if (args_num == 0) {
        puts("I need more arguments to run, see help for details");
        return false;
    }

    int argument = (int)strtol(args[0], NULL, 10);
    printf("Blurring (intensity: %d)...\n", argument);

    image_blur(img, argument);

    return true;
}

bool exec_mirror(image* img, int args_num, char** args) {
    if (args_num == 0) {
        puts("I need more arguments to run, see help for details");
        return false;
    }

    bool x = strchr(args[0], 'X') || strchr(args[0], 'x');
    bool y = strchr(args[0], 'Y') || strchr(args[0], 'y');
    
    if (!x && !y) {
        puts("Could not find valid axis letter");
        return exec_do_nothing(img, args_num, args);
    }

    printf("Mirroring (%s%s)...\n", x ? "x" : "", y ? (x ? " and y" : "y") : "");

    image_mirror(img, x, y);

    return true;
}

bool exec_do_nothing(image* img, int args_num, char** args) {
    puts("Doing nothing...");
    return true; // Done nothing successfully
}

operation* operations_find_by_name(char* name) {
    operation* current = start_entry;
    while (current && strcmp(name, current->name) != 0)
        current = current->next;
    return current;
}

operation* operations_get() {
    return start_entry;
}