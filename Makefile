all: main.o
	gcc -o lab6 main.o image.o image_io.o operations.o image_utils.o -lm

main.o: main.c operations.o image_io.o
	gcc -c -o main.o main.c

operations.o: image_utils.o operations.c
	gcc -c -o operations.o operations.c

image_utils.o: image.o image_utils.c
	gcc -c -o image_utils.o image_utils.c

image_io.o: image.o image_io.c
	gcc -c -o image_io.o image_io.c

image.o: image.c
	gcc -c -o image.o image.c

clean:
	rm *.o lab6 2> /dev/null || true