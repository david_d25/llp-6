#ifndef _IMAGE_H
#define _IMAGE_H

#include <stdint.h>

struct image;
typedef struct image image;

typedef struct pixel {
    uint8_t r, g, b, a;
} pixel;

typedef struct point {
    uint32_t x, y;
} point;

image* image_create(uint32_t, uint32_t);

void image_destroy(image*);

image* image_copy(image*);

pixel image_get_pixel(image*, point);
pixel image_get_pixel_or_default(image*, point, pixel);

void image_set_pixel(image*, point, pixel);

uint32_t image_get_width(image*);
uint32_t image_get_height(image*);
void image_swap_size(image*);

#endif