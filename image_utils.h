#ifndef _IMAGE_UTILS_H
#define _IMAGE_UTILS_H

#include <stdint.h>

#include "image.h"

void image_blur(image*, int);
void image_rotate(image*, double);
void image_mirror(image*, bool, bool);

#endif