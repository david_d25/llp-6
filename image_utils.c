#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>
#include <errno.h>
#include <malloc.h>
#include <math.h>
#include <stdbool.h>

#include "image_utils.h"

void matrix3_multiply(double* m1, double* m2, double* dst) {
    dst[0] = m1[0]*m2[0] + m1[1]*m2[3] + m1[2]*m2[6];
    dst[1] = m1[0]*m2[1] + m1[1]*m2[4] + m1[2]*m2[7];
    dst[2] = m1[0]*m2[2] + m1[1]*m2[5] + m1[2]*m2[8];

    dst[3] = m1[3]*m2[0] + m1[4]*m2[3] + m1[5]*m2[6];
    dst[4] = m1[3]*m2[1] + m1[4]*m2[4] + m1[5]*m2[7];
    dst[5] = m1[3]*m2[2] + m1[4]*m2[5] + m1[5]*m2[8];

    dst[6] = m1[6]*m2[0] + m1[7]*m2[3] + m1[8]*m2[6];
    dst[7] = m1[6]*m2[1] + m1[7]*m2[4] + m1[8]*m2[7];
    dst[8] = m1[6]*m2[2] + m1[7]*m2[5] + m1[8]*m2[8];
}

point matrix3_apply(double* matrix3, point p) {
    return (point) {
        floor(matrix3[0]*(p.x + 0.5) + matrix3[1]*(p.y + 0.5) + matrix3[2]),
        floor(matrix3[3]*(p.x + 0.5) + matrix3[4]*(p.y + 0.5) + matrix3[5])
    };
}

void image_rotate(image* img, double radians) {
    bool size_swap_needed = fmod(fabs(radians) + M_PI_4, M_PI) > M_PI_2;
    printf("%f\n", fmod(abs(radians) + M_PI_4, M_PI));

    uint32_t img_width = image_get_width(img);
    uint32_t img_height = image_get_height(img);

    double rot_mat3_reverse[] = {
        cos(-radians), -sin(-radians), 0,
        sin(-radians), cos(-radians), 0,
        0, 0, 1
    };

    double center_x = (double)img_width/2.0;
    double center_y = (double)img_height/2.0;

    double center_mat3[] = {
        1, 0, -center_x,
        0, 1, -center_y,
        0, 0, 1
    };

    double uncenter_mat3[] = {
        1, 0, center_x,
        0, 1, center_y,
        0, 0, 1
    };

    double size_swap_correction_mat3[] = {
        1, 0, 0,
        0, 1, 0,
        0, 0, 1
    };

    image* src_img = image_copy(img);

    if (size_swap_needed) {
        image_swap_size(img);
        img_width = image_get_width(img);
        img_height = image_get_height(img);

        size_swap_correction_mat3[2] = center_x - center_y;
        size_swap_correction_mat3[5] = center_y - center_x;
    }

    double rot_center_mat3[9];
    double rot_full_mat3[9];
    double transform_mat3[9];

    matrix3_multiply(uncenter_mat3, rot_mat3_reverse, rot_center_mat3);
    matrix3_multiply(rot_center_mat3, center_mat3, rot_full_mat3);
    matrix3_multiply(rot_full_mat3, size_swap_correction_mat3, transform_mat3);

    for (uint32_t x = 0; x < img_width; x++) {
        for (uint32_t y = 0; y < img_height; y++) {
            point dst = {x, y};
            point src = matrix3_apply(transform_mat3, dst);
            pixel src_pixel = image_get_pixel(src_img, src);
            image_set_pixel(img, dst, src_pixel);
        }
    }
    image_destroy(src_img);
}

void image_blur(image* img, int pixels) {
    image* src_img = image_copy(img);

    uint32_t img_width = image_get_width(img);
    uint32_t img_height = image_get_height(img);

    for (int64_t x = 0; x < img_width; x++) {
        for (int64_t y = 0; y < img_height; y++) {
            point src = {x, y};

            double r = 0, g = 0, b = 0;
            double divider = 0;

            for (int64_t bx = x - pixels; bx <= x + pixels; bx++) {

                for (int64_t by = y - pixels; by <= y + pixels; by++) {
                    int64_t eff_x = bx < 0 ? 0 : bx >= img_width ? img_width - 1 : bx;
                    int64_t eff_y = by < 0 ? 0 : by >= img_height ? img_height - 1 : by;

                    double dsq = (eff_x - x)*(eff_x - x)*(eff_y - y)*(eff_y - y);
                    double weight = exp(-dsq/(2*pixels*pixels))/(M_PI*2*pixels*pixels);
                    
                    pixel p = image_get_pixel(src_img, (point) {eff_x, eff_y});
                    r += p.r * weight;
                    g += p.g * weight;
                    b += p.b * weight;
                    divider += weight;
                }

            }

            image_set_pixel(img, src, (pixel) {r/divider, g/divider, b/divider});
        }
    }
    image_destroy(src_img);
}

void image_mirror(image* img, bool mirror_x, bool mirror_y) {
    image* src_img = image_copy(img);

    uint32_t img_width = image_get_width(img);
    uint32_t img_height = image_get_height(img);

    for (int64_t x = 0; x < img_width; x++) {
        for (int64_t y = 0; y < img_height; y++) {
            image_set_pixel(
                img,
                (point) {
                    mirror_x ? img_width - x - 1 : x,
                    mirror_y ? img_height - y - 1 : y
                },
                image_get_pixel(src_img, (point) {x, y})
            );
        }
    }
    image_destroy(src_img);
}